# L1 Frontend serial interface
set_property LOC G15 [get_ports frontend_l1_cs]
set_property LOC G16 [get_ports frontend_l1_sclk]
set_property LOC D22 [get_ports frontend_l1_sdata]
set_property IOSTANDARD LVCMOS33 [get_ports frontend_l1_*]