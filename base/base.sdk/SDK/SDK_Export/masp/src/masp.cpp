/**
 * MASP Payload main file
 */

#include "xgpiops.h"

#include "MAX2769.hpp"

void print( const char* );

static XGpioPs gpio;


int main(){
    print("Starting...\n\r");

	// Initialise the GPIO
    XGpioPs_Config *GpioConfigPtr = XGpioPs_LookupConfig( XPAR_PS7_GPIO_0_DEVICE_ID );
    XGpioPs_CfgInitialize( &gpio, GpioConfigPtr, GpioConfigPtr->BaseAddr );
    print("Initialised GPIO\n\r");

    // Initialise the L1 frontend
    MAX2769 l1( &gpio, 55, 56, 57 );
    print("Initialised L1 frontend interface\n\r");

    // Setup L1 frontend
    l1.setRegister( 0b0000, 0b1010001010010001100110100011 );	// Configuration 1
    l1.setRegister( 0b0001, 0b0000010101010000001010001000 ); 	// Configigure AGC and output sections
    l1.setRegister( 0b0010, 0b1110101011111110000111011100 ); 	// Configures support and test functions for IF filter and AGC
    l1.setRegister( 0b0011, 0b1001111011000000000000001000 ); 	// Configures PLL, VCO, and CLK settings
    l1.setRegister( 0b0100, 0b0001100111001110000010101000 ); 	// Cofigures PLL fractional division ratio, other controls
    l1.setRegister( 0b0111, 0b1000000000001000000000000110 ); 	// Configureof Fraction clock0divider values

    print("Done\n\r");
    while( true ){}
}
